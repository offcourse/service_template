(ns offcourse.{{ns-name}}.config
    (:require [cljs.nodejs :as node]
              [offcourse.{{ns-name}}.specs :refer [specs]]
              [shared.protocols.convertible :as cv]))

(def config {:name :{{ns-name}}
             :adapters {}
             :specs specs})
