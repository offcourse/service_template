(ns offcourse.{{ns-name}}.core
  (:require [backend-shared.service.index :as service]
            [offcourse.{{ns-name}}.config :refer [config]]
            [shared.protocols.eventful :as ev])
  (:require-macros [cljs.core.async.macros :refer [go]]))


(defn ^:export handler [& args]
  (go
    (let [service  (service/create config args)]
      (ev/respond service [:received (:event service)]))))

(defn -main [] identity)
(set! *main-cli-fn* -main)
