(ns leiningen.new.offcourse_service
  (:use [leiningen.new.templates :only [renderer name-to-path sanitize-ns ->files]]))

(def render (renderer "offcourse_service"))

(defn offcourse_service
  [name]
  (let [data {:name name
              :ns-name (sanitize-ns name)
              :sanitized (name-to-path name)}]
    (->files data ["src/offcourse/{{sanitized}}/core.cljs" (render "src/offcourse/echo/core.cljs" data)]
["src/offcourse/{{sanitized}}/fetch.cljs" (render "src/offcourse/echo/fetch.cljs" data)]
["src/offcourse/{{sanitized}}/specs.cljs" (render "src/offcourse/echo/specs.cljs" data)]
["project.clj" (render "project.clj" data)]
["src/offcourse/{{sanitized}}/perform.cljs" (render "src/offcourse/echo/perform.cljs" data)]
["src/offcourse/{{sanitized}}/config.cljs" (render "src/offcourse/echo/config.cljs" data)]
["event.json" (render "event.json")]
["index.js" (render "index.js")]
["context.json" (render "context.json")]
)))